package diploma.application;

import lombok.NoArgsConstructor;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.fingerprint.Fingerprinter;
import org.openscience.cdk.fingerprint.IBitFingerprint;

@NoArgsConstructor
public class Comparator {

    public double compare(AtomContainer molecule1, AtomContainer molecule2){
        return 0;
    }

    public static IBitFingerprint getFingerprint(AtomContainer molecule) throws CDKException {
        Fingerprinter fingerprinter = new Fingerprinter();
        return fingerprinter.getBitFingerprint(molecule);
    }
}
