package diploma.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.fingerprint.IBitFingerprint;
import org.openscience.cdk.similarity.Tanimoto;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.BitSet;

@Controller
public class ApplicationController {

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private ServletContext context;

/*
    @RequestMapping(value = "/uploadfile", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity handleFileUpload(@RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                String uploadsDir = "/uploads/";
                String realPathtoUploads = context.getRealPath(uploadsDir);
                if (!new File(realPathtoUploads).exists()) {
                    new File(realPathtoUploads).mkdir();
                }



                String orgName = file.getOriginalFilename();
                String filePath = realPathtoUploads + orgName;
                File dest = new File(filePath);
                file.transferTo(dest);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }*/

    @RequestMapping("/")
    public String index(Model model){
        model.addAttribute("first", "/first");
        model.addAttribute("second", "/second");
        model.addAttribute("third", "/third");
        model.addAttribute("fourth", "/fourth");

        return "index";
    }

    @RequestMapping("/first")
    public String first(Model model) throws IOException, CDKException {
        model.addAttribute("name", "Two different molecules");

        AtomContainer mol1 = MolReaderWriter.read("/home/natalia/MolExamples/1.MOL");
        AtomContainer mol2 = MolReaderWriter.read("/home/natalia/MolExamples/2.MOL");
        IBitFingerprint fing = Comparator.getFingerprint(mol1);
        BitSet molbits1 = fing.asBitSet();
        fing = Comparator.getFingerprint(mol2);
        BitSet molbits2 = fing.asBitSet();
        model.addAttribute("tanimoto", Tanimoto.calculate(molbits1, molbits2));
        String image1 = MolReaderWriter.renderImage("/home/natalia/MolExamples/1.MOL");
        String image2 = MolReaderWriter.renderImage("/home/natalia/MolExamples/2.MOL");
        model.addAttribute("image1", image1);
        model.addAttribute("image2", image2);
        String smiles1 = MolReaderWriter.getSmiles("/home/natalia/MolExamples/1.MOL");
        String smiles2 = MolReaderWriter.getSmiles("/home/natalia/MolExamples/2.MOL");
        model.addAttribute("smiles1", smiles1);
        model.addAttribute("smiles2",smiles2);
        return "comparison";
    }

    @RequestMapping("/second")
    public String second(Model model) throws IOException, CDKException {
        model.addAttribute("name", "Two different molecules");

        AtomContainer mol1 = MolReaderWriter.read("/home/natalia/MolExamples/1.MOL");
        AtomContainer mol2 = MolReaderWriter.read("/home/natalia/MolExamples/1copy.MOL");
        IBitFingerprint fing = Comparator.getFingerprint(mol1);
        BitSet molbits1 = fing.asBitSet();
        fing = Comparator.getFingerprint(mol2);
        BitSet molbits2 = fing.asBitSet();
        model.addAttribute("tanimoto", Tanimoto.calculate(molbits1, molbits2));
        String image1 = MolReaderWriter.renderImage("/home/natalia/MolExamples/1.MOL");
        String image2 = MolReaderWriter.renderImage("/home/natalia/MolExamples/1copy.MOL");
        model.addAttribute("image1", image1);
        model.addAttribute("image2", image2);
        String smiles1 = MolReaderWriter.getSmiles("/home/natalia/MolExamples/1.MOL");
        String smiles2 = MolReaderWriter.getSmiles("/home/natalia/MolExamples/1copy.MOL");
        model.addAttribute("smiles1", smiles1);
        model.addAttribute("smiles2",smiles2);
        return "comparison";
    }

    @RequestMapping("/third")
    public String third(Model model) throws IOException, CDKException {
        model.addAttribute("name", "Two different molecules");

        AtomContainer mol1 = MolReaderWriter.read("/home/natalia/MolExamples/4-1.MOL");
        AtomContainer mol2 = MolReaderWriter.read("/home/natalia/MolExamples/4-2.MOL");
        IBitFingerprint fing = Comparator.getFingerprint(mol1);
        BitSet molbits1 = fing.asBitSet();
        fing = Comparator.getFingerprint(mol2);
        BitSet molbits2 = fing.asBitSet();
        model.addAttribute("tanimoto", Tanimoto.calculate(molbits1, molbits2));
        String image1 = MolReaderWriter.renderImage("/home/natalia/MolExamples/4-1.MOL");
        String image2 = MolReaderWriter.renderImage("/home/natalia/MolExamples/4-2.MOL");
        model.addAttribute("image1", image1);
        model.addAttribute("image2", image2);
        String smiles1 = MolReaderWriter.getSmiles("/home/natalia/MolExamples/4-1.MOL");
        String smiles2 = MolReaderWriter.getSmiles("/home/natalia/MolExamples/4-2.MOL");
        model.addAttribute("smiles1", smiles1);
        model.addAttribute("smiles2",smiles2);
        return "comparison";
    }

    @RequestMapping("/fourth")
    public String fourth(Model model) throws IOException, CDKException {
        model.addAttribute("name", "Two different molecules");

        AtomContainer mol1 = MolReaderWriter.read("/home/natalia/MolExamples/5-1.MOL");
        AtomContainer mol2 = MolReaderWriter.read("/home/natalia/MolExamples/5-2.MOL");
        IBitFingerprint fing = Comparator.getFingerprint(mol1);
        BitSet molbits1 = fing.asBitSet();
        fing = Comparator.getFingerprint(mol2);
        BitSet molbits2 = fing.asBitSet();
        model.addAttribute("tanimoto", Tanimoto.calculate(molbits1, molbits2));
        String image1 = MolReaderWriter.renderImage("/home/natalia/MolExamples/5-1.MOL");
        String image2 = MolReaderWriter.renderImage("/home/natalia/MolExamples/5-2.MOL");
        model.addAttribute("image1", image1);
        model.addAttribute("image2", image2);
        String smiles1 = MolReaderWriter.getSmiles("/home/natalia/MolExamples/5-1.MOL");
        String smiles2 = MolReaderWriter.getSmiles("/home/natalia/MolExamples/5-2.MOL");
        model.addAttribute("smiles1", smiles1);
        model.addAttribute("smiles2",smiles2);
        return "comparison";
    }

    @RequestMapping("/greeting")
    public String greeting(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

}