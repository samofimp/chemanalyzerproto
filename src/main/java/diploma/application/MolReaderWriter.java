package diploma.application;

import lombok.NoArgsConstructor;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.io.MDLV2000Reader;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.layout.StructureDiagramGenerator;
import org.openscience.cdk.renderer.AtomContainerRenderer;
import org.openscience.cdk.renderer.font.AWTFontManager;
import org.openscience.cdk.renderer.generators.BasicAtomGenerator;
import org.openscience.cdk.renderer.generators.BasicBondGenerator;
import org.openscience.cdk.renderer.generators.BasicSceneGenerator;
import org.openscience.cdk.renderer.visitor.AWTDrawVisitor;
import org.openscience.cdk.smiles.SmiFlavor;
import org.openscience.cdk.smiles.SmilesGenerator;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

@NoArgsConstructor
public class MolReaderWriter {
    public static AtomContainer read(String path) throws FileNotFoundException, CDKException {

        FileReader reader = new FileReader(path);
        MDLV2000Reader molreader = new MDLV2000Reader(reader);
        AtomContainer molecule = new AtomContainer();
        molreader.read(molecule);

        return molecule;
    }

    public static String renderImage(String path) throws IOException, CDKException {
        AtomContainer molecule = read(path);
        int WIDTH = 1000;
        int HEIGHT = 1000;

        Rectangle drawArea = new Rectangle(WIDTH, HEIGHT);
        Image image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        StructureDiagramGenerator sdg = new StructureDiagramGenerator();
        sdg.setMolecule(molecule);
        try {
            sdg.generateCoordinates();
        } catch (Exception ex) {
            //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

        ArrayList generators = new ArrayList();
        generators.add(new BasicSceneGenerator());
        generators.add(new BasicBondGenerator());
        generators.add(new BasicAtomGenerator());

        AtomContainerRenderer renderer = new AtomContainerRenderer(generators, new AWTFontManager());

        renderer.setup(molecule, drawArea);

        Graphics2D g2 = (Graphics2D) image.getGraphics();
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, WIDTH, HEIGHT);

        renderer.paint(molecule, new AWTDrawVisitor(g2));

        String[] partsOfName = path.split("\\.");

        partsOfName = partsOfName[0].split("/");
        String newName = partsOfName[partsOfName.length - 1] + ".png";
        String saveName = "./src/main/webapp/uploads/" + newName;

        File file = new File(saveName);
        ImageIO.write((RenderedImage)image, "PNG", file);
        return "/uploads/" + newName;
    }

    public static String getSmiles(String path) throws FileNotFoundException, CDKException {
        AtomContainer molecule = read(path);
        SmilesGenerator generator = new SmilesGenerator(SmiFlavor.Generic);
        return generator.create(molecule);
    }
}

